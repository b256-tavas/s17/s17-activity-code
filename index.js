// console.log("hello")

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	let fullName = prompt("What is your name?")
	console.log("Hello, " + fullName)

	let age = prompt("How old are you?")
	console.log("You are " + age + " years old.")

	let address = prompt("Where do you live?")
	console.log("You live in " + address + ".")

	function thankYouMessage () {
		alert("Thank you for your input!")
	}

	thankYouMessage();
	
	//first function here:

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.

*/

	function favoriteBands() {
		
		let band1 = "1. The Beatles";
		let band2 = "2. Metallica";
		let band3 = "3. The Eagles";
		let band4 = "4. L'arc~en~Ciel";
		let band5 = "5. Eraserheads";

		console.log(band1);
		console.log(band2);
		console.log(band3);
		console.log(band4);
		console.log(band5);
	}

	favoriteBands();
	//second function here:


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	function favoriteMovies() {
		
		let movie1 = "1. The Godfather";
		let movie2 = "2. The Godfather, Part II";
		let movie3 = "3. Shawshank Redemption";
		let movie4 = "4. To Kill a Mockingbird";
		let movie5 = "5. Psycho";

		console.log(movie1);
		console.log("Rotten Tomatoes Rating: 97%")
		console.log(movie2);
		console.log("Rotten Tomatoes Rating: 96%")
		console.log(movie3);
		console.log("Rotten Tomatoes Rating: 91%")
		console.log(movie4);
		console.log("Rotten Tomatoes Rating: 93%")
		console.log(movie5);
		console.log("Rotten Tomatoes Rating: 96%")
	}

	favoriteMovies();
	//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


function printFriends() {
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");


	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

// console.log(friend1);
// console.log(friend2);